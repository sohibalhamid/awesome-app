package com.projects.awesomeapp.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class SimpleRecyclerAdapter<T> extends RecyclerView.Adapter<SimpleRecyclerAdapter.SimpleViewHolder> {
    protected List<T> mainData;
    private @LayoutRes
    int layoutRes;
    private SimpleRecyclerAdapter.OnViewHolder<T> listener;

    public SimpleRecyclerAdapter(List<T> mainData, int layoutRes, OnViewHolder<T> listener) {
        this.mainData = mainData;
        this.layoutRes = layoutRes;
        this.listener = listener;
    }

    public interface OnViewHolder<T>{
        void onBindView(SimpleViewHolder holder, T item);
    }

    public static class SimpleViewHolder extends RecyclerView.ViewHolder{
        private SimpleRecyclerAdapter.OnViewHolder listener;
        private ViewDataBinding layoutBinding;

        public SimpleViewHolder(@NonNull View itemView, SimpleRecyclerAdapter.OnViewHolder listener) {
            super(itemView);

            try {
                this.listener = listener;
                layoutBinding = DataBindingUtil.bind(itemView);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        public ViewDataBinding getLayoutBinding(){
            return layoutBinding;
        }
    }

    @NonNull
    @Override
    public SimpleRecyclerAdapter.SimpleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(getLayoutRes(), parent, false);
        return new SimpleViewHolder(view, getListener());
    }

    @Override
    public void onBindViewHolder(@NonNull SimpleRecyclerAdapter.SimpleViewHolder holder, int position) {
        T t = mainData.get(position);
        getListener().onBindView(holder, t);
    }

    @Override
    public int getItemCount() {
        return getMainData() == null ? 0 : getMainData().size();
    }

    public void setMainData(List<T> mainData) {
        this.mainData = mainData;
        notifyDataSetChanged();
    }
}
