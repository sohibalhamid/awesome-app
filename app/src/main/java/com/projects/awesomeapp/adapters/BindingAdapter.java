package com.projects.awesomeapp.adapters;

import android.widget.ImageView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.projects.awesomeapp.helpers.GridSpacingItemDecorationUtil;
import com.projects.awesomeapp.helpers.UIHelper;

public class BindingAdapter {
    @androidx.databinding.BindingAdapter({"sourceCompat"})
    public static void setImageSrcCompat(ImageView imageView, int imageResource) {
        imageView.setImageResource(imageResource);
    }

    @androidx.databinding.BindingAdapter({"setAdapter"})
    public static void setAdapter(RecyclerView recyclerView, SimpleRecyclerAdapter<?> adapter) {
        recyclerView.setAdapter(adapter);
    }

    @androidx.databinding.BindingAdapter("setupGridRecyclerView")
    public static void setupGridRecyclerView(RecyclerView recyclerView, float margin){
        recyclerView.setLayoutManager(
                new GridLayoutManager(recyclerView.getContext(), 2)
        );
        recyclerView.addItemDecoration(
                new GridSpacingItemDecorationUtil(margin)
        );
    }

    @androidx.databinding.BindingAdapter("setupHorizontalRecyclerView")
    public static void setupHorizontalRecyclerView(RecyclerView recyclerView, float margin){
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), RecyclerView.HORIZONTAL, false));
        recyclerView.addItemDecoration(
                new GridSpacingItemDecorationUtil(margin, "horizontal")
        );
    }

    @androidx.databinding.BindingAdapter("setupVerticalRecyclerView")
    public static void setupVerticalRecyclerView(RecyclerView recyclerView, float margin){
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), RecyclerView.VERTICAL, false));
        recyclerView.addItemDecoration(
                new GridSpacingItemDecorationUtil(margin, "vertical")
        );
    }
}
