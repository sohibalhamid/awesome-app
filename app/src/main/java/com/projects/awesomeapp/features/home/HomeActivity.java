package com.projects.awesomeapp.features.home;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.projects.awesomeapp.R;
import com.projects.awesomeapp.adapters.SimpleRecyclerAdapter;
import com.projects.awesomeapp.databinding.ActivityHomeBinding;
import com.projects.awesomeapp.databinding.ItemPhotoGridBinding;
import com.projects.awesomeapp.databinding.ItemPhotoListBinding;
import com.projects.awesomeapp.features.BaseActivity;
import com.projects.awesomeapp.features.bottomSheets.BottomSheetNotification;
import com.projects.awesomeapp.features.photoDetail.PhotoDetailActivity;
import com.projects.awesomeapp.helpers.ConnectionDetectorHelper;
import com.projects.awesomeapp.helpers.GlobalVariable;
import com.projects.awesomeapp.models.PhotoModel;
import com.projects.awesomeapp.connections.requests.GetPhotoListRequest;

import java.util.ArrayList;

public class HomeActivity extends BaseActivity<ActivityHomeBinding> {

    private int countPage;
    private boolean isHasMorePage;
    private boolean isError;
    private boolean loading;
    private PhotoViewModel photoViewModel;
    private SimpleRecyclerAdapter<PhotoModel> photoAdapterGrid;
    private SimpleRecyclerAdapter<PhotoModel> photoAdapterList;

    @Override
    protected int attachLayout() {
        return R.layout.activity_home;
    }

    @Override
    protected void initData() {
        super.initData();
        countPage = 1;
        isHasMorePage = true;
        loading = true;

        photoAdapterGrid = new SimpleRecyclerAdapter<>(
                new ArrayList<>(),
                R.layout.item_photo_grid,
                (holder, item) -> {
                    ItemPhotoGridBinding binding = (ItemPhotoGridBinding) holder.getLayoutBinding();
                    binding.setPhotoModel(item);
                    Glide.with(this).asBitmap().load(item.getMedium()).into(binding.fieldPhoto);
                    binding.item.setOnClickListener(onCLick -> {
                        Intent intent = new Intent(HomeActivity.this, PhotoDetailActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putSerializable(GlobalVariable.KEY_PHOTO_DETAIL, item);
                        intent.putExtras(bundle);
                        startActivity(intent);
                    });
                }
        );

        photoAdapterList = new SimpleRecyclerAdapter<>(
                new ArrayList<>(),
                R.layout.item_photo_list,
                (holder, item) -> {
                    ItemPhotoListBinding binding = (ItemPhotoListBinding) holder.getLayoutBinding();
                    binding.setPhotoModel(item);
                    Glide.with(this).asBitmap().load(item.getMedium()).into(binding.fieldPhoto);
                    binding.item.setOnClickListener(onCLick -> {
                        Intent intent = new Intent(HomeActivity.this, PhotoDetailActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putSerializable(GlobalVariable.KEY_PHOTO_DETAIL,item);
                        intent.putExtras(bundle);
                        startActivity(intent);
                    });
                }
        );

        getPhotoList();
    }

    @Override
    protected void initLayout() {
        super.initLayout();

        binding.setGridAdapter(photoAdapterGrid);
        binding.setListAdapter(photoAdapterList);
        binding.setIsGridView(true);
    }

    @Override
    protected void initAction() {
        super.initAction();

        binding.icMenuGrid.setOnClickListener(onCLick -> {
            binding.setIsGridView(true);
            binding.fieldGrid.setVisibility(View.VISIBLE);
            binding.fieldList.setVisibility(View.GONE);
        });
        binding.icMenuList.setOnClickListener(onClick -> {
            binding.setIsGridView(false);
            binding.fieldGrid.setVisibility(View.GONE);
            binding.fieldList.setVisibility(View.VISIBLE);
        });

        binding.rvGrid.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                GridLayoutManager layoutManager = (GridLayoutManager) binding.rvGrid.getLayoutManager();
                assert layoutManager != null;
                int lastItemVisible =  layoutManager.findLastVisibleItemPosition();
                int currentTotalCount = layoutManager.getItemCount();
                if (isHasMorePage && loading){
                    if ((currentTotalCount-2) <= lastItemVisible){
                        loading = false;
                        countPage++;
                        getPhotoListFromServer();
                    }
                }
            }
        });

        binding.rvList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) binding.rvList.getLayoutManager();
                assert linearLayoutManager != null;
                int lastItemVisible =  linearLayoutManager.findLastVisibleItemPosition();
                int currentTotalCount = linearLayoutManager.getItemCount();
                if (isHasMorePage && loading){
                    if ((currentTotalCount-2) <= lastItemVisible){
                        loading = false;
                        countPage++;
                        getPhotoListFromServer();
                    }
                }
            }
        });
    }

    private void getPhotoList(){
        binding.progressBar.setVisibility(View.VISIBLE);
        if (ConnectionDetectorHelper.newInstance().checkConnection(this)){
            getPhotoListFromServer();
        } else {
            isError = true;
            getPhotoListFromDatabase();
        }
    }

    private void getPhotoListFromServer(){
        GetPhotoListRequest request = GetPhotoListRequest.builder()
                .page(countPage)
                .perPage(GlobalVariable.PAGE_SIZE)
                .build();

        photoViewModel = ViewModelProviders.of(this).get(PhotoViewModel.class);
        photoViewModel.getPhotoList(request);
        photoViewModel.getIsHasMorePage().observe(this, response -> isHasMorePage = response);
        photoViewModel.getPhotoLiveData().observe(this, response -> {
            binding.progressBar.setVisibility(View.GONE);
            loading = true;
            if (response.isEmpty()){
                if (isError) popupNotification(getString(R.string.message_error));
                else popupNotification(getString(R.string.message_empty));
            } else {
                photoViewModel.insertPhotoListToDatabase(response);
                photoAdapterGrid.setMainData(response);
                photoAdapterList.setMainData(response);
            }
        });
    }

    private void getPhotoListFromDatabase(){
        GetPhotoListRequest request = GetPhotoListRequest.builder()
                .page(countPage)
                .perPage(GlobalVariable.PAGE_SIZE)
                .build();

        photoViewModel = ViewModelProviders.of(this).get(PhotoViewModel.class);
        photoViewModel.getPhotoListFromDatabase(request);
        photoViewModel.getPhotoLiveData().observe(this, response -> {
            binding.progressBar.setVisibility(View.GONE);
            loading = true;
            if (response.isEmpty()){
                if (isError) popupNotification(getString(R.string.message_error));
                else popupNotification(getString(R.string.message_empty));
            } else {
                photoAdapterGrid.setMainData(response);
                photoAdapterList.setMainData(response);
            }
        });
    }

    private void popupNotification(String description){
        new BottomSheetNotification.Builder()
                .setDescription(description)
                .build(this)
                .show();
    }
}