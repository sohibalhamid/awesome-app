package com.projects.awesomeapp.models;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import org.json.JSONObject;

import java.io.Serializable;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity(indices = @Index(value = "id", unique = true))
public class PhotoModel implements Serializable {

    @PrimaryKey()
    @SerializedName("id")
    @ColumnInfo(name = "id")
    private int id;

    @SerializedName("photographer")
    @ColumnInfo(name = "photographer")
    private String photographer;

    @SerializedName("photographer_id")
    @ColumnInfo(name = "photographer_id")
    private String photographerId;

    @SerializedName("photographer_url")
    @ColumnInfo(name = "photographer_url")
    private String photographerUrl;

    @SerializedName("original")
    @ColumnInfo(name = "original")
    private String original;

    @SerializedName("medium")
    @ColumnInfo(name = "medium")
    private String medium;

    public PhotoModel(JSONObject jsonObject){
        try {
            if (jsonObject.has("id")){
                setId(jsonObject.getInt("id"));
            }
            if (jsonObject.has("photographer")){
                setPhotographer(jsonObject.getString("photographer"));
            }
            if (jsonObject.has("photographer_id")){
                setPhotographerId(jsonObject.getString("photographer_id"));
            }
            if (jsonObject.has("photographer_url")){
                setPhotographerUrl(jsonObject.getString("photographer_url"));
            }
            if (jsonObject.has("original")){
                setOriginal(jsonObject.getString("original"));
            }
            if (jsonObject.has("medium")){
                setMedium(jsonObject.getString("medium"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
