package com.projects.awesomeapp.helpers;

import android.graphics.Rect;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class GridSpacingItemDecorationUtil extends RecyclerView.ItemDecoration{
    private final int mItemOffset;
    private String type;

    public GridSpacingItemDecorationUtil(float itemOffsetFloat, String type){
        this.mItemOffset = (int) itemOffsetFloat;
        this.type = type;
    }

    public GridSpacingItemDecorationUtil(float mItemOffset) {
        this.mItemOffset = (int) mItemOffset;
    }

    @Override
    public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);

        if (type == null)
            outRect.set(mItemOffset, mItemOffset, mItemOffset, mItemOffset);
        else{
            if (type.equalsIgnoreCase(GlobalVariable.KEY_ORIENTASI_VERTICAL)){
                if (parent.getChildAdapterPosition(view) == 0){
                    outRect.set(0, mItemOffset, 0, mItemOffset);
                } else {
                    outRect.set(0, 0, 0, mItemOffset);
                }
            }
            if (type.equalsIgnoreCase(GlobalVariable.KEY_ORIENTASI_HORIZONTAL)){
                if (parent.getChildAdapterPosition(view) == 0){
                    outRect.set(mItemOffset, 0, mItemOffset, 0);
                } else {
                    outRect.set(0, 0, mItemOffset, 0);
                }
            }
        }
    }
}
