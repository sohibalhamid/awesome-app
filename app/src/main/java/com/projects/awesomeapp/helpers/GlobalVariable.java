package com.projects.awesomeapp.helpers;

public class GlobalVariable {
    public static final String KEY_URL_HEADER = "Authorization";
    public static final String KEY_URL_API_KEY = "563492ad6f91700001000001b3814af475e9447db2a1c75f93fb7ed8";
    public static final String KEY_URL_PAGE = "page";
    public static final String KEY_URL_PER_PAGE = "per_page";
    public static final Integer PAGE_SIZE = 10;

    public static final String KEY_ORIENTASI_VERTICAL = "vertical";
    public static final String KEY_ORIENTASI_HORIZONTAL = "horizontal";
    public static final String KEY_PHOTO_DETAIL = "photo_detail";
}
