package com.projects.awesomeapp.connections.networks;

import androidx.annotation.Nullable;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.projects.awesomeapp.helpers.GlobalVariable;
import com.projects.awesomeapp.connections.requests.BaseRequest;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class UrlConverterRequest extends JsonObjectRequest {
    private final JSONObject jsonObject;

    public UrlConverterRequest(int method, String url, BaseRequest request, Response.Listener<JSONObject> listener, @Nullable Response.ErrorListener errorListener) {
        super(method, convertUrl(method, url, request.generateJSONParameter()), request.generateJSONParameter(), listener, errorListener);
        this.jsonObject = request.generateJSONParameter();
    }

    private static String convertUrl(int method, String url, JSONObject jsonRequest){
        if (method == Method.GET) {
            if (jsonRequest != null) {
                StringBuilder parameter = new StringBuilder();
                Iterator<String> keys = jsonRequest.keys();
                while (keys.hasNext()) {
                    String key = keys.next();
                    try {
                        String value = jsonRequest.getString(key);
                        parameter.append(key).append("=").append(value);
                        if (keys.hasNext()) {
                            parameter.append("&");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                return url + "?" + parameter;
            }
            else {
                return url;
            }
        } else {
            return url;
        }
    }

    @Override
    public Map<String, String> getHeaders() {
        Map<String, String> headers = new HashMap<>();
        headers.put(GlobalVariable.KEY_URL_HEADER, GlobalVariable.KEY_URL_API_KEY);
        return headers;
    }

    @Nullable
    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        try {
            return jsonToStringMap(this.jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
            return super.getParams();
        }
    }

    private Map<String, String> jsonToStringMap(JSONObject object) throws JSONException {
        Map<String, String> map = new HashMap<>();

        Iterator<String> keysItr = object.keys();
        while (keysItr.hasNext()){
            String key = keysItr.next();
            String value = (String) object.get(key);

            map.put(key, value);
        }
        return map;
    }
}
