package com.projects.awesomeapp.connections.databases;

import android.app.Activity;
import android.content.Context;

import androidx.annotation.Nullable;

import com.projects.awesomeapp.GlobalApp;
import com.projects.awesomeapp.connections.requests.GetPhotoListRequest;
import com.projects.awesomeapp.connections.responses.DatabaseResponseErrorListener;
import com.projects.awesomeapp.connections.responses.DatabaseResponseSuccessListener;
import com.projects.awesomeapp.models.PhotoModel;

import java.util.List;

public class PhotoDatabase implements InterfacePhotoListDatabase{
    private @Nullable
    Activity currentActivity;
    private final PhotoDatabaseDefinition databaseDefinition;

    public PhotoDatabase(@Nullable Context context) {
        if (context != null){
            if (context instanceof Activity){
                this.currentActivity = (Activity) context;
            }
        }
        this.databaseDefinition = GlobalApp.getNewInstance().getDatabaseDefinition();
    }

    @Override
    public void insertPhoto(PhotoModel photoModel, DatabaseResponseSuccessListener<Long> successListener, DatabaseResponseErrorListener errorListener) {
        new Thread(){
            @Override
            public void run() {
                Long id = databaseDefinition.photoDao().insert(photoModel);
                successListener.setResult(id);

                if (currentActivity != null){
                    currentActivity.runOnUiThread(successListener);
                    return;
                }
                successListener.run();
            }
        }.start();
    }

    @Override
    public void getPhotoList(GetPhotoListRequest request, DatabaseResponseSuccessListener<List<PhotoModel>> successListener, DatabaseResponseErrorListener errorListener) {
        new Thread(){
            @Override
            public void run() {
                List<PhotoModel> photoModels = databaseDefinition.photoDao()
                        .getMovieList(request.getPage(), request.getPerPage());

                successListener.setResult(photoModels);

                if (currentActivity != null){
                    currentActivity.runOnUiThread(successListener);
                    return;
                }
                successListener.run();
            }
        }.start();
    }
}
