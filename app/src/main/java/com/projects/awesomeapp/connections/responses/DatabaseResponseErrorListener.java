package com.projects.awesomeapp.connections.responses;

public abstract class DatabaseResponseErrorListener implements Runnable {
    @Override
    public void run() {

    }

    public abstract void onErrorResponseListener();
}
