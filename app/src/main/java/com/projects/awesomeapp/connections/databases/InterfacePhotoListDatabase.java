package com.projects.awesomeapp.connections.databases;

import com.projects.awesomeapp.connections.requests.GetPhotoListFromDatabase;
import com.projects.awesomeapp.connections.requests.GetPhotoListRequest;
import com.projects.awesomeapp.connections.responses.DatabaseResponseErrorListener;
import com.projects.awesomeapp.connections.responses.DatabaseResponseSuccessListener;
import com.projects.awesomeapp.models.PhotoModel;

import java.util.List;

public interface InterfacePhotoListDatabase {

    void insertPhoto(PhotoModel photoModel,
                     DatabaseResponseSuccessListener<Long> successListener,
                     DatabaseResponseErrorListener errorListener);

    void getPhotoList(GetPhotoListRequest request,
                      DatabaseResponseSuccessListener<List<PhotoModel>> successListener,
                      DatabaseResponseErrorListener errorListener);
}
