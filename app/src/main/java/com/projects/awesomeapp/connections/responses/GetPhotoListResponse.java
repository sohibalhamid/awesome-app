package com.projects.awesomeapp.connections.responses;

import com.projects.awesomeapp.models.PhotoModel;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;

@Getter
public class GetPhotoListResponse {
    private final List<PhotoModel> photoModelList = new ArrayList<>();

    public GetPhotoListResponse(JSONObject response){
        try {
            if (response.has("photos")){
                JSONArray jsonArray = response.getJSONArray("photos");
                for (int index = 0; index < jsonArray.length(); index++){
                    JSONObject jsonObject = jsonArray.getJSONObject(index);
                    PhotoModel photoModel = new PhotoModel(jsonObject);
                    if (jsonObject.has("src")){
                        JSONObject src = jsonObject.getJSONObject("src");
                        photoModel.setMedium(src.getString("medium"));
                        photoModel.setOriginal(src.getString("original"));
                    }
                    photoModelList.add(photoModel);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
